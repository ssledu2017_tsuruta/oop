# ネコが魚を食べる-Python

class Cat:
    # メソッド：食べる
    def eat(self, fish):
        pass

class Fish:
    pass

# Catクラスからインスタンスcatを生成
cat = Cat()

# Fishクラスからインスタンスfishを生成
fish = Fish()

# catがfishを食べる
cat.eat(fish)
