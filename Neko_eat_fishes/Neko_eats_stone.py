# ネコが魚を食べる
from enum import Enum

# おなかの状態(列挙型)
class Stomach(Enum):
    GOOD = 0
    BAD = 1

class Putable:
    pass

class Food(Putable):
    pass

class Fish(Food):
    pass

class Meat(Food):
    pass

class Stone(Putable):
    pass

class Cat():
    def __init__(self):
        self.__stomach = Stomach.GOOD
    # メソッド：食べる
    def eat(self, obj):
        if isinstance(obj,Putable):
            if isinstance(obj,Stone):
                self.__pain()
            elif isinstance(obj,Food):
                pass
    # メソッド：痛くなる
    def __pain(self):
        self.stomach = Stomach.BAD
        print(self.stomach.name)

# Catクラスからインスタンスcatを生成
cat = Cat()

# Fishクラスからインスタンスfishを生成
fish = Fish()

# Meatクラスからインスタンスmeatを生成
meat = Meat()

# Stoneクラスからインスタンスstoneを生成
stone = Stone()

# catがfishを食べる
cat.eat(fish)

# catがmeatを食べる
cat.eat(meat)

# catがstoneを食べる
cat.eat(stone)
