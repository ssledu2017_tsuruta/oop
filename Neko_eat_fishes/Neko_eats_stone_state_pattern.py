
### Stateパターン実装Ver　ネコは石を食べるとお腹が痛くなる -Python ###

# ネコクラス
class Cat:
    def __init__(self):
        self.stomach = GoodStomachState()
    # メソッド:食べる
    def eat(self, obj):
        if isinstance(obj,Putable):
            self.stomach = self.stomach.eat(obj)
    # メソッド：云う
    def talk(self):
        pass

# おなかの状態
class StomachState:
    # メソッド:食べる
    def eat(self):
        pass
    # メソッド:云う
    def talk(self):
        pass
# 良い状態
class GoodStomachState(StomachState):
    # メッソッド：食べる
    def eat(self,obj):
        self.talk()
        if isinstance(obj,Stone):
            return self.pain()
        else:
            return GoodStomachState()
    # メソッド:痛くなる
    def pain(self):
        return BadStomachState()
    # メソッド:云う
    def talk(self):
        print("ごちそうさま")
# 悪い状態
class BadStomachState(StomachState):
    # メソッド:食べる
    def eat(self,obj):
        self.talk()
        return self.vomit()
    # メソッド:吐く
    def vomit(self):
        return GoodStomachState()
    # メソッド:云う
    def talk(self):
        print("オエー")

# 口に入れられるものクラス
class Putable:
    pass

# 食べ物クラス
class Food(Putable):
    pass

# 魚クラス
class Fish(Food):
    pass

# 肉クラス
class Meat(Food):
    pass

# 石クラス
class Stone(Putable):
    pass

# Catクラスからインスタンスcatを作成
cat = Cat()
# Fishクラスからインスタンスfishを作成
fish = Fish()
# Meatクラスからインスタンスmeatを作成
meat = Meat()
# Stoneクラスからインスタンスstoneを作成
stone = Stone()


cat.eat(fish)

cat.eat(stone)

cat.eat(meat)
