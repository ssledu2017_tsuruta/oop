# ネコが魚をとってきて食べる-Python
class Neko:
    # おなかの状態と名前の初期化
    def __init__(self,name):
        self.onaka = 'hungry'
        self.name = name
    # メソッド：食べる
    def eat(self, osakana):
        if osakana.having == self.name:
            print("----{0} eats {1}----".format(self.name, self.get))
            self.onaka = 'full'
            osakana.eaten()
            osakana.having = 'nobody'
        else:
            print("---{0} did not get {1}.---".format(self.name, osakana.name))
    # メソッド：状態を知らせる
    def state(self):
        print("{0} is {1}.".format(self.name, self.onaka))

class Sakana:
    # 生死の状態と名前の初期化
    def __init__(self, name):
        self.inochi = 'alive'
        self.name = name
        self.having = 'nobody'
    # メソッド：食べられる
    def eaten(self):
        self.inochi = 'dead'
    # メソッド：狩られる
    def hunted(self, hunter):
        self.having = hunter
    # メソッド：状態を知らせる
    def state(self):
        print("{0} has been hunted by {1}.".format(self.name, self.hunted))
        print("{0} is {1}.".format(self.name, self.inochi))

class WildNeko(Neko):
    def __init__(self, name):
        super(). __init__(name)
        self.get = 'nothing'
    def hunt(self, osakana):
        self.get = osakana.name
        osakana.hunted(self.name)
    def state(self):
        super().state()
        print("{0} gets {1}".format(self.name, self.get))

class HouseNeko(Neko):
    def __init__(self, name, master):
        super().__init__(name)
        self.master = master.name
        master.pet = self.name
    def require(self, master):
        pass
    def state(self):
        super().state()
        print("{0} is had by {1}".format(self.name,self.master))
class Owner:
    def __init__(self, name):
        self.name = name
        self.pet = 'nothing'
    def give(self):
        pass
    def state(self):
        pass


# # WildNekoクラスからインスタンスnoranekoを生成
# noraneko = WildNeko("野良ネコさん")
# noraneko.state()
# # Sakanaクラスからインスタンスsalmonを生成
# salmon = Sakana("サケ")
# salmon.state()
# # noranekoがsalmonを狩る
# noraneko.hunt(salmon)
# noraneko.state()
# # noranekoがsalmonを食べる
# noraneko.eat(salmon)
# noraneko.state()

# Ownerクラスからインスタンスtarouを生成
tarou = Owner("太郎さん")
tarou.state()
# HouseNekoクラスからインスタンスkainekoを生成
kaineko = HouseNeko("飼いネコさん",tarou)
kaineko.state()
